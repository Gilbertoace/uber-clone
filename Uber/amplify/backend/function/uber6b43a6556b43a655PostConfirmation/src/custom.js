const aws = require('aws-sdk');
const ddb = new aws.DynamoDB();

exports.handler = async (event, context) => {
  // event is all the options that this function receives when it is being called
  if (!event.request.userAttributes.sub) {
    console.log('Error: No user was written to DynamoDB');

    // return back to cognito from lambda
    context.done(null, event);
    return;
  }

  // if we have a sub, save user to DynamoDB
  const date = new Date();

  const params = {
    // item needed to be saved
    item: {
      // S stands for string
      id: {S: event.request.userAttributes.sub},
      _typename: {S: 'User'}, //property for graphQL to know what type this is
      username: {S: event.userName},
      email: {S: event.request.userAttributes.email},
      createdAt: {S: date.toISOString()},
      updatedAt: {S: date.toISOString()},
    },
    TableName: process.env.USERTABLE,
  };

  try {
    await ddb.putItem(params).promise();
    console.log('Success');
  } catch (error) {
    console.log('Error', error);
  }

  context.done(null, event);
};

