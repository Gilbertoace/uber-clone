import React, {useState, useEffect} from 'react';
import {Text, View, TextInput, SafeAreaView, Pressable} from 'react-native';
import PlaceRow from './PlaceRow';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const DestinationSearch = () => {
  const [originPlace, setOriginPlace] = useState(null);
  const [destinationPlace, setDestinationPlace] = useState(null);

  const navigation = useNavigation();

  const routeToSearch = () => {
    //data is passed as params to the routed component, it can be accessed using useRoute().params
    if (originPlace !== null && destinationPlace !== null) {
      return navigation.navigate('SearchResults', {
        originPlace, destinationPlace
      });
    }

    return console.warn('Input is empty');
  };

  const homePlace = {
    description: 'Home',
    geometry: {location: {lat: 48.8152937, lng: 2.4597668}},
  };
  const workPlace = {
    description: 'Work',
    geometry: {location: {lat: 48.8496818, lng: 2.2940881}},
  };

  //   if originPlace and destinationPlace is not null, redirect to another UI page
  //   useEffect(() => {
  //     if (originPlace && destinationPlace) {
  //       console.warn('Page redirection is a go');
  //       routeToSearch()
  //     }
  //   }, [originPlace, destinationPlace]);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <GooglePlacesAutocomplete
          placeholder="Where From?"
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            // console.log(data, details);
            setOriginPlace({data, details});
          }}
          currentLocation={true}
          currentLocationLabel="Current location"
          suppressDefaultStyles
          styles={{
            textInput: styles.textInput,
            container: styles.autocompleteContainer,
            listView: styles.listView,
            separator: styles.separator,
          }}
          enablePoweredByContainer={false}
          fetchDetails
          query={{
            key: 'AIzaSyA0zJF4H_ovl_FQn3hhnhGiFNmArDAwNQY',
            language: 'en',
          }}
          renderRow={() => <PlaceRow data={data} />}
          renderDescription={data => data.description || data.vicinity}
          predefinedPlaces={[homePlace, workPlace]}
        />

        <GooglePlacesAutocomplete
          placeholder="Where to?"
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            // console.log(data, details);
            setDestinationPlace({data, details});
          }}
          enablePoweredByContainer={false}
          suppressDefaultStyles
          styles={{
            textInput: styles.textInput,
            container: {
              ...styles.autocompleteContainer,
              top: 55,
            },
            separator: styles.separator,
          }}
          fetchDetails
          query={{
            key: 'AIzaSyA0zJF4H_ovl_FQn3hhnhGiFNmArDAwNQY',
            language: 'en',
          }}
          renderRightButton={() => (
            <Pressable
              onPress={routeToSearch}
              style={{
                backgroundColor: 'black',
                padding: 10,
                margin: 10,
                alignItems: 'center',
                width: 100,
                position: 'absolute',
                top: 60,
                right: 0,
                borderRadius: 5,
              }}>
              <Text style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
                Route
              </Text>
            </Pressable>
          )}
        />

        <View style={styles.circle} />
        <View style={styles.line} />
        <View style={styles.square} />
      </View>
    </SafeAreaView>
  );
};

export default DestinationSearch;
