import React, {useState} from 'react';
import {StyleSheet, Text, View, Dimensions, Alert} from 'react-native';
import {API, graphqlOperation, Auth} from 'aws-amplify';
import UberTypes from '../../components/UberTypes';
import RouteMap from '../../components/RouteMap';
import {createOrder} from '../../graphql/mutations';

import {useRoute, useNavigation} from '@react-navigation/native';

const SearchResult = () => {
  // keeping track of the type of car selected by user
  const typeState = useState(null);

  // here we make use of the route.params to access data sent through useNavigation
  const route = useRoute();

  const navigation = useNavigation();

  const {originPlace, destinationPlace} = route.params;

  const onSubmit = async () => {
    const [type] = typeState; //same as typeState[0].type
    if (!type) {
      return;
    }

    try {
      // current user
      const userInfo = await Auth.currentAuthenticatedUser();

      // current date
      const date = new Date();

      const input = {
        createdAt: date.toISOString(),
        type: type,
        originLatitude: originPlace.details.geometry.location.lat,
        originLongitude: originPlace.details.geometry.location.lng,

        destLatitude: destinationPlace.details.geometry.location.lat,
        destLongitude: destinationPlace.details.geometry.location.lng,

        userId: userInfo.attributes.sub,
        carId: "1",
      };

      const response = API.graphql(
        graphqlOperation(createOrder, {
          input,
        }),
      );

      
      Alert.alert("Created", "Order has been submitted", [{
        text: "Go home",
        onPress: ()=> navigation.navigate('Home'),
      }])

      return response;
      
    } catch (error) {
      console.error(e);
    }
  };

  return (
    <View style={{display: 'flex', justifyContent: 'space-between'}}>
      <View style={{height: Dimensions.get('window').height - 430}}>
        <RouteMap origin={originPlace} destination={destinationPlace} />
      </View>

      <View style={{height: 430}}>
        <UberTypes typeState={typeState} onSubmit={onSubmit} />
      </View>
    </View>
  );
};

export default SearchResult;

const styles = StyleSheet.create({});
