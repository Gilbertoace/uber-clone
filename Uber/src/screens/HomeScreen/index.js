import React from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';
import HomeMap from '../../components/HomeMap';
import CovidMessage from '../../components/CovidMessage';
import HomeSearch from '../../components/HomeSearch';

const HomeScreen = (props) => {
    return (
        <View style={{ height: Dimensions.get('window').height - 430 }}>
            <HomeMap />
            <CovidMessage />
            <HomeSearch />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {}
})

export default HomeScreen;