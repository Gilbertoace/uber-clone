import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const CovidMessage = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Travel only if necessary</Text>
      <Text style={styles.text}>
        Very interesting indeed. I hear this solution very often, proposed
        for many different code ailments....and it has NEVER worked as a
        solution for me. Cheers!
      </Text>
      <Text style={styles.learnMore}>Learn more</Text>
    </View>
  );
};

export default CovidMessage;
