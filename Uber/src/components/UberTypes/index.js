import React from 'react';
import {Text, View, Pressable, ScrollView} from 'react-native';
import styles from './styles';
import UberTypeRow from '../UberTypeRow';

import typesData from '../../assets/data/types';

const UberTypes = ({typeState, onSubmit}) => {
  const [selectType, setSelectedType] = typeState;
  // const confirm = () => {
  //   console.warn('confirm');
  // };
  return (
    <ScrollView>
      <View>
        {typesData.map(type => (
          <UberTypeRow key={type.id} type={type} isSelected={type.type === selectType} onPress={()=> setSelectedType(type.type)} />
        ))}

        <Pressable
          onPress={onSubmit}
          style={{
            backgroundColor: 'black',
            padding: 10,
            margin: 10,
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontWeight: 'bold'}}>Confirm Uber</Text>
        </Pressable>
      </View>
    </ScrollView>
  );
};

export default UberTypes;
