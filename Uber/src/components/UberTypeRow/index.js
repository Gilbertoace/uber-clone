import React from 'react';
import {Text, View, Image} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import styles from './styles';

const UberTypeRow = props => {
  const {type, onPress, isSelected} = props;
  // const {type, price, duration} = type;

  const getImage = () => {
    switch (type) {
      case 'UberX':
        return require('../../assets/images/UberX.jpeg');
      case 'Comfort':
        return require('../../assets/images/Comfort.jpeg');
      case 'UberXL':
        return require('../../assets/images/UberXL.jpeg');

      default:
        require('../../assets/images/UberXL.jpeg');
    }
  };
  return (
    <Pressable
      onPress={onPress}
      style={[
        styles.container,
        {backgroundColor: isSelected ? '#efefef' : '#fff'},
      ]}>
      <Image style={styles.image} source={getImage()} />
      <View style={styles.middleContainer}>
        <Text style={styles.type}>
          {type.type} <Ionicons name="person" size={12} />3
        </Text>
        <Text style={styles.time}>8:03PM drop off</Text>
      </View>
      <View style={styles.rightContainer}>
        <Ionicons name="pricetag" size={18} color="#42d742" />
        <Text style={styles.price}>est. ${type.price}</Text>
      </View>
    </Pressable>
  );
};

export default UberTypeRow;
