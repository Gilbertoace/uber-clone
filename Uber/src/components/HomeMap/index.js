import React, { useState, useEffect } from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
// import cars from '../../assets/data/cars';
import { API, graphqlOperation} from 'aws-amplify';
import {listCars} from '../../graphql/queries';

const HomeMap = () => {
  const [cars, setCars] = useState([]);
  // Get images for cars...

  useEffect(()=>{
    const fetchCars = async () => {
      try {
        const response = await API.graphql(
          graphqlOperation(
            listCars
          )
        )
        setCars(response.data.listCars.items);
      } catch (error) {
        console.error(error);
      }
    }
    fetchCars();
  }, [])
  const getImage = type => {
    switch (type) {
      case 'UberX':
        return require('../../assets/images/top-UberX.png');
      case 'Comfort':
        return require('../../assets/images/top-Comfort.png');
      case 'UberXL':
        return require('../../assets/images/top-UberXL.png');

      default:
        require('../../assets/images/top-UberXL.png');
    }
  };
  return (
    <MapView
      style={{height: '100%', width: '100%'}}
      provider={PROVIDER_GOOGLE}
      showsUserLocation={true}
      initialRegion={{
        latitude: 28.450627,
        longitude: -16.263045,
        latitudeDelta: 0.0222,
        longitudeDelta: 0.0121,
      }}>
      {cars.map(({type, id, latitude, longitude, heading}) => (
        <Marker
          key={id}
          coordinate={{latitude: latitude, longitude: longitude}}>
          <Image
            style={{
              width: 50,
              height: 50,
              resizeMode: 'contain',
              transform: [
                {
                  rotate: `${heading}deg`,
                },
              ],
            }}
            source={getImage(type)}
          />
        </Marker>
      ))}
    </MapView>
  );
};

export default HomeMap;

const styles = StyleSheet.create({});
