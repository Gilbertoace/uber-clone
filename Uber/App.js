import 'react-native-gesture-handler';

import React, { useEffect } from 'react';
import {StyleSheet, StatusBar, PermissionsAndroid, Platform} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import Router from './src/Navigation/Root';
import Amplify from 'aws-amplify';
import config from './src/aws-exports';

import { withAuthenticator } from 'aws-amplify-react-native';


navigator.geolocation = require('@react-native-community/geolocation');

Amplify.configure(config);
const App = (props) => {

  // android permission to use location in an android phone...
  const androidPermissions = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Uber App Camera Permission",
          message:
            "Uber App needs access to your camera " +
            "so you can take awesome rides.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
      } else {
        console.log("Location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  useEffect(()=> {
    if(Platform.OS === 'android'){
      androidPermissions();
    }else {
      // IOS
      Geolocation.requestAuthorization();
    }
  }, [])
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Router/>
    </>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default withAuthenticator(App);
